package com.djkj.activiti.mapper;

import com.djkj.activiti.bean.ActivitiVariable;

import java.util.List;

public interface TblActivitiMapper {

    public ActivitiVariable select(ActivitiVariable activitiVariable);

    public List<ActivitiVariable> selectList(ActivitiVariable activitiVariable);

    public int insertData(ActivitiVariable activitiVariable);

    public int update(ActivitiVariable activitiVariable);

    public int delete(Integer id);
}
