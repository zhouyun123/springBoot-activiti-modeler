package com.djkj.activiti.service;

import com.djkj.activiti.bean.ProcessManage;

import java.util.List;

public interface ProcessManageService {
    
    public List<ProcessManage> selectList(ProcessManage processManage);

    public int insert(ProcessManage processManage);

    public int delete(int id);

    public int update(ProcessManage processManage);
}
