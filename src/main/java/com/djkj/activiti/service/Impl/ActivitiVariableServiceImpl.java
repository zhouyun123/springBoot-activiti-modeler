package com.djkj.activiti.service.Impl;

import com.djkj.activiti.bean.ActivitiVariable;
import com.djkj.activiti.mapper.TblActivitiMapper;
import com.djkj.activiti.service.ActivitiVariableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("activitiVariableService")
public class ActivitiVariableServiceImpl implements ActivitiVariableService {

    @Autowired
    private TblActivitiMapper tblActivitiMapper;

    @Override
    public ActivitiVariable select(ActivitiVariable activitiVariable) {
        return tblActivitiMapper.select(activitiVariable);
    }

    @Override
    public List<ActivitiVariable> selectList(ActivitiVariable activitiVariable) {
        return tblActivitiMapper.selectList(activitiVariable);
    }

    @Override
    public int insert(ActivitiVariable activitiVariable) {
        return tblActivitiMapper.insertData(activitiVariable);
    }

    @Override
    public int update(ActivitiVariable activitiVariable) {
        return tblActivitiMapper.update(activitiVariable);
    }

    @Override
    public int delete(Integer id) {
        return tblActivitiMapper.delete(id);
    }
}
