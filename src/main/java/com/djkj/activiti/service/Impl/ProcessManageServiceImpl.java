package com.djkj.activiti.service.Impl;

import com.djkj.activiti.bean.ProcessManage;
import com.djkj.activiti.mapper.ProcessManageMapper;
import com.djkj.activiti.service.ProcessManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ProcessManageServiceImpl implements ProcessManageService {

    @Resource
    private ProcessManageMapper processManageMapper;

    @Override
    public List<ProcessManage> selectList(ProcessManage processManage) {
        return processManageMapper.selectList(processManage);
    }

    @Override
    public int insert(ProcessManage processManage) {
        return processManageMapper.insert(processManage);
    }

    @Override
    public int delete(int id) {
        return processManageMapper.delete(id);
    }

    @Override
    public int update(ProcessManage processManage) {
        return processManageMapper.update(processManage);
    }

}
