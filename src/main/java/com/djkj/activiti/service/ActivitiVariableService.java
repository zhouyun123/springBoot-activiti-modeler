package com.djkj.activiti.service;

import com.djkj.activiti.bean.ActivitiVariable;

import java.util.List;

public interface ActivitiVariableService {

    public ActivitiVariable select(ActivitiVariable activitiVariable);

    public List<ActivitiVariable> selectList(ActivitiVariable activitiVariable);

    public int insert(ActivitiVariable activitiVariable);

    public int update(ActivitiVariable activitiVariable);

    public int delete(Integer id);

}
