package com.djkj.activiti.service;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.task.Task;

import java.util.List;

public interface ActivitiService {

    /*启动流程*/
    public void startProcesses(String id,String business_key);

    /*根据用户id查询待办任务列表*/
    public List<Task> findTasksByUserId(String userId);

    /*任务审批 	（通过/拒接）*/
    public void completeTask(String taskId,String userId,String result);

    /*更改业务流程状态*/
    public void updateBizStatus(DelegateExecution execution, String status);

    /*流程节点权限用户列表*/
    public List<String> findUsersForSL(DelegateExecution execution);

    /*流程节点权限用户列表*/
    public List<String> findUsersForSP(DelegateExecution execution);

    /*生成流程图*/
    public void queryProImg(String processInstanceId) throws Exception;

    /*流程图高亮显示*/
    public String queryProHighLighted(String processInstanceId) throws Exception;

    public Task findTaskById(String taskId);

}
