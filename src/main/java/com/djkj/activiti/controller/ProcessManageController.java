package com.djkj.activiti.controller;

import com.djkj.activiti.bean.ProcessManage;
import com.djkj.activiti.service.ProcessManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/process")
public class ProcessManageController {
    @Autowired
    private ProcessManageService processManageService;


    @RequestMapping("/list")
    public String list(ModelMap modelMap){
        ProcessManage processManage=new ProcessManage();
        List<ProcessManage> list = processManageService.selectList(processManage);
        modelMap.addAttribute("list",list);
        return "process/list";
    }
}
