package com.djkj.activiti.common;

import com.djkj.activiti.bean.ActivitiVariable;
import com.djkj.activiti.service.ActivitiVariableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.Servlet;

public class BusinessTaskUtil {


    public void actBusiness_applyForm(String userId,String business_Id,String result){
        ActivitiVariable activitiVariable = new ActivitiVariable();
        activitiVariable.setId(Integer.parseInt(business_Id));
        activitiVariable.setApplyStr(result);
        ActivitiVariableService activitiVariableService =
                (ActivitiVariableService)ApplicationContextUtil.getBeanById("activitiVariableService");
        activitiVariableService.update(activitiVariable);
    }

    public void actBusiness_approveForm(String userId,String business_Id,String result){
        ActivitiVariable activitiVariable = new ActivitiVariable();
        activitiVariable.setId(Integer.parseInt(business_Id));
        activitiVariable.setApproveStr(result);
        ActivitiVariableService activitiVariableService =
                (ActivitiVariableService)ApplicationContextUtil.getBeanById("activitiVariableService");
        activitiVariableService.update(activitiVariable);
    }

}
