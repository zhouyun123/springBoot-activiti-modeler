/*
Navicat MySQL Data Transfer

Source Server         : ceshi
Source Server Version : 50560
Source Host           : 127.0.0.1:3306
Source Database       : activiti

Target Server Type    : MYSQL
Target Server Version : 50560
File Encoding         : 65001

Date: 2019-02-20 13:27:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_process_manage
-- ----------------------------
DROP TABLE IF EXISTS `tbl_process_manage`;
CREATE TABLE `tbl_process_manage` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `process_type` varchar(255) DEFAULT NULL COMMENT '流程类别',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
