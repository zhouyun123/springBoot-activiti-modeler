/*
Navicat MySQL Data Transfer

Source Server         : ceshi
Source Server Version : 50560
Source Host           : 127.0.0.1:3306
Source Database       : activiti

Target Server Type    : MYSQL
Target Server Version : 50560
File Encoding         : 65001

Date: 2019-02-19 15:46:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_activiti
-- ----------------------------
DROP TABLE IF EXISTS `tbl_activiti`;
CREATE TABLE `tbl_activiti` (
  `id` int(50) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `applyStr` varchar(255) DEFAULT NULL COMMENT '申请内容',
  `approveStr` varchar(255) DEFAULT NULL COMMENT '审批内容',
  PRIMARY KEY (`id`),
  UNIQUE KEY `pk_tbl_activiti` (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
